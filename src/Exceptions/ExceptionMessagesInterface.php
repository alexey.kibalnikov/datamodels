<?php
/**
 * ExceptionMessagesInterface.php
 * @author Alexey Kibalnikov (alexey.kibalnikov@gmail.com)
 * @copyright (c) iWeekender 2019
 * @package DataModels\Exceptions
 */

namespace iWeekender\DataModels\Exceptions;

/**
 * Runtime Error Messages (for logged).
 *
 * Runtime errors <b>will be hidden</b> from the user and the response API will not be given.
 */
interface ExceptionMessagesInterface
{
    const MES_DATA_MODEL_COLLECTION_MAP_CLASS = "DataModelCollection PropertyMapsClass is Empty";
    const MES_CLASS_NOT_IMPLEMENT_INTERFACE   = "Class '%s' not implement interface '%s'";
    const MES_TRANSFORMATION_NOT_SUPPORTED    = "Transfornation not supproted";
}
