<?php
/**
 * AbstractDataModelElement.php
 * @author Alexey Kibalnikov (alexey.kibalnikov@gmail.com)
 * @copyright (c) iWeekender 2019
 * @package DataModels
 */

namespace iWeekender\DataModels;

use iWeekender\DataModels\Exceptions\ExceptionMessagesInterface as EM;
use iWeekender\Contract\DataModels\DataModelInterface;
use iWeekender\Exceptions\IWException;
use ReflectionClass;

/**
 * Class AbstractDataModelElement
 */
abstract class AbstractDataModelElement extends AbstractDataModel
{
    /**
     * @var array
     */
    protected $propertyMapsToAssociativeArray = [];

    /**
     * @var array
     */
    protected $propertyMapsClass = [];

    /**
     * @inheritDoc
     * @throws IWException
     */
    public function __construct() {
        parent::__construct();

        foreach ($this->propertyMapsClass as $property => $class) {
            $setter = 'set' . $property;
            $object = new $class();
            if (!$object instanceof DataModelInterface) {
                throw new IWException(
                    sprintf(
                        EM::MES_CLASS_NOT_IMPLEMENT_INTERFACE,
                        $property::__CLASS__,
                        DataModelInterface::class
                    )
                );
            }
            $this->$setter($object);
        }
    }

    /**
     * @inheritDoc
     */
    public function getAssociativeArray(): array {
        $result = [];
        $rc = new ReflectionClass(static::class);
        $list = $rc->getProperties();
        foreach ($list as $prop) {
            $prop->setAccessible(true);
            if (in_array($prop->name, $this->propertyMapsToAssociativeArray)) {
                $key = $this->getAssociativeArrayKey($prop->name);
                $value = $prop->getValue($this);
                if (is_object($value)) {
                    $value = $value->getAssociativeArray();
                }
                $result[$key] = $value;
            }
        }
        return $result;
    }

    /**
     * @inheritDoc
     */
    public function loadToObject(array $array): void {
        $associativeArrayMapsToProperty = [];
        foreach ($this->propertyMapsToAssociativeArray as $property) {
            $key = $this->getAssociativeArrayKey($property);
            $associativeArrayMapsToProperty[$key] = $property;
        }

        foreach ($array as $key => $value) {
            if (array_key_exists($key, $associativeArrayMapsToProperty)) {
                $property = $associativeArrayMapsToProperty[$key];
                $getter = method_exists($this, 'get'.$property) ? 'get'.$property : 'is'.$property;
                $setter = 'set'.$property;

                if (is_object($this->$getter())) {
                    $object = $this->$getter();
                    /** @var AbstractDataModel $object */
                    $object->loadToObject($value);
                    $this->$setter($object);
                } else {
                    $this->$setter($value);
                }
            }
        }
    }
}
