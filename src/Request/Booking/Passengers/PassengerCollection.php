<?php
/**
 * PassengerCollection.php
 * @author Alexey Kibalnikov (alexey.kibalnikov@gmail.com)
 * @copyright (c) iWeekender 2019
 * @package DataModels\Request\Params\Passenger
 */

namespace iWeekender\DataModels\Request\Booking\Passengers;

use iWeekender\Contract\ImportExport\DataFormatEnumInterface;
use iWeekender\Contract\Request\Booking\Passengers\PassengerCollectionInterface;
use iWeekender\Contract\Request\Booking\Passengers\PassengerInterface;
use iWeekender\Utils\ImportExport\AbstractImport;
use Exception;
use ArrayIterator;
use Traversable;

/**
 * Class PassengerCollection
 */
class PassengerCollection extends AbstractImport implements PassengerCollectionInterface
{
    const IMPORT_FORMAT = DataFormatEnumInterface::JSON;

    /**
     * @var Passenger[]
     */
    private $collection = [];

    /**
     * @var string
     */
    private $contactPhone;

    /**
     * @var string
     */
    private $contactEmail;

    public function __construct() {
        parent::__construct(self::IMPORT_FORMAT);
    }

    /**
     * @param array $associativeArray
     * @throws Exception
     */
    public function loadToObject(array $associativeArray): void {
        foreach ($associativeArray['travelers'] as $data) {
            $passenger = new Passenger();
            $passenger->loadToObject($data);
            $this->collection[] = $passenger;
        }

        $this->contactPhone = $associativeArray['contact']['phone'];
        $this->contactEmail = $associativeArray['contact']['email'];
    }

    /**
     * @param mixed $offset
     * @return bool
     */
    public function offsetExists($offset) {
        return isset($this->collection[$offset]);
    }

    /**
     * @param mixed $offset
     * @return Passenger|mixed|null
     */
    public function offsetGet($offset) {
        return isset($this->collection[$offset]) ? $this->collection[$offset] : null;
    }

    /**
     * @param mixed $offset
     * @param mixed $value
     */
    public function offsetSet($offset, $value) {
        if (is_null($offset)) {
            $this->collection[] = $value;
        } else {
            $this->collection[$offset] = $value;
        }
    }

    /**
     * @param mixed $offset
     */
    public function offsetUnset($offset) {
        unset($this->collection[$offset]);
    }

    /**
     * @return int|void
     */
    public function count() {
        return count($this->collection);
    }

    /**
     * @return ArrayIterator|Traversable
     */
    public function getIterator() {
        return new ArrayIterator($this->collection);
    }

    /**
     * @inheritDoc
     */
    public function getPrimaryPassenger(): PassengerInterface {
        $result = null;
        foreach ($this->collection as $passenger) {
            if ($passenger->isPrimaryTravelerType()) {
                $result = $passenger;
                break;
            }
        }
        return $result;
    }

    /**
     * @param string $ageGradation
     * @return int
     */
    private function getCountByAgeGradation(string $ageGradation): int {
        $result = 0;
        foreach ($this->collection as $passenger) {
            if ($passenger->getAgeGradation() === $ageGradation) {
                $result++;
            }
        }
        return $result;
    }

    /**
     * @inheritDoc
     */
    public function getAdultsCount(): int {
        return $this->getCountByAgeGradation(Passenger::AGE_GRADAION_ADULT);
    }

    /**
     * @inheritDoc
     */
    public function getChildrenCount(): int {
        return $this->getCountByAgeGradation(Passenger::AGE_GRADAION_CHILDREN);
    }

    /**
     * @inheritDoc
     */
    public function getInfantsCount(): int {
        return $this->getCountByAgeGradation(Passenger::AGE_GRADAION_INFANT);
    }

    /**
     * @inheritDoc
     */
    public function getContactPhone(): string {
        return $this->contactPhone;
    }

    /**
     * @inheritDoc
     */
    public function getContactEmail(): string {
        return $this->contactEmail;
    }
}
