<?php
/**
 * Passenger.php
 * @author Alexey Kibalnikov (alexey.kibalnikov@gmail.com)
 * @copyright (c) iWeekender 2019
 * @package DataModels\Request\Params\Passenger
 */

namespace iWeekender\DataModels\Request\Booking\Passengers;

use iWeekender\Contract\Format\DateTimeInterface;
use iWeekender\Contract\ImportExport\DataFormatEnumInterface;
use iWeekender\Contract\Request\Booking\Passengers\PassengerInterface;
use iWeekender\Utils\ImportExport\AbstractImport;
use DateTime;
use DateTimeZone;
use Exception;

/**
 * Class Passenger
 */
class Passenger extends AbstractImport implements PassengerInterface
{
    const IMPORT_FORMAT = DataFormatEnumInterface::JSON;

    /**
     * @var string
     */
    private $firstName;

    /**
     * @var string
     */
    private $lastName;

    /**
     * @var string
     */
    private $nationality;

    /**
     * @var string
     */
    private $gender;

    /**
     * @var string
     */
    private $birthday;

    /**
     * @var string
     */
    private $passport;

    /**
     * @var string
     */
    private $expiredDate;

    /**
     * @var string
     */
    private $travelerType = '';

    /**
     * @var int
     */
    private $age;

    public function __construct() {
        parent::__construct(self::IMPORT_FORMAT);
    }

    /**
     * @inheritDoc
     * @throws Exception
     */
    public function loadToObject(array $associativeArray): void {
        $this->firstName    = $associativeArray['first_name'];
        $this->lastName     = $associativeArray['last_name'];
        $this->nationality  = $associativeArray['nationality'];
        $this->gender       = $associativeArray['gender'];
        $this->birthday     = (string)substr($associativeArray['dob'], 0, 10);
        $this->passport     = $associativeArray['pid'];
        $this->expiredDate  = (string)substr($associativeArray['pidexp'], 0, 10);
        $this->travelerType = $associativeArray['traveler_type'];

        $dateTime = DateTime::createFromFormat(
            DateTimeInterface::IW_DATE_FORMAT,
            $this->birthday,
            new DateTimeZone('Europe/London')
        );

        $now = new DateTime('@' . time());
        $this->age = $now->format('Y') - $dateTime->format('Y');
    }

    /**
     * @inheritDoc
     */
    public function getFirstName(): string {
        return $this->firstName;
    }

    /**
     * @inheritDoc
     */
    public function getLastName(): string {
        return $this->lastName;
    }

    /**
     * @inheritDoc
     */
    public function getNationality(): string {
        return $this->nationality;
    }

    /**
     * @inheritDoc
     */
    public function getGender(): string {
        return $this->gender;
    }

    /**
     * @inheritDoc
     */
    public function getBirthday(): string {
        return $this->birthday;
    }

    /**
     * @inheritDoc
     */
    public function getPassport(): string {
        return $this->passport;
    }

    /**
     * @inheritDoc
     */
    public function getExpiredDate(): string {
        return $this->expiredDate;
    }

    /**
     * @inheritDoc
     */
    public function isPrimaryTravelerType(): bool {
        return $this->travelerType === self::TRAVELER_TYPE_PRIMARY;
    }

    /**
     * @inheritDoc
     */
    public function getAge(): int {
        return $this->age;
    }

    /**
     * @inheritDoc
     */
    public function getAgeGradation(): string {
        if ($this->age >= 16) {
            return self::AGE_GRADAION_ADULT;
        } elseif ($this->age >= 2) {
            return self::AGE_GRADAION_CHILDREN;
        } else {
            return self::AGE_GRADAION_INFANT;
        }
    }
}
