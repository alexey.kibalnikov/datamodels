<?php
/**
 * Price.php
 * @author Alexey Kibalnikov (alexey.kibalnikov@gmail.com)
 * @copyright (c) iWeekender 2019
 * @package DataModels\Common
 */

namespace iWeekender\DataModels\Common;

use iWeekender\DataModels\AbstractDataModelElement;
use iWeekender\Contract\DataModels\Common\PriceInterface;

/**
 * Class Price
 */
final class Price extends AbstractDataModelElement implements PriceInterface
{
    protected $propertyMapsToAssociativeArray = [
        'id',
        'usd',
        'eur'
    ];

    /**
     * @var int|null
     */
    private $id;

    /**
     * @var string|null
     */
    private $usd = self::DEFAULT_PRICE;

    /**
     * @var string|null
     */
    private $eur = self::DEFAULT_PRICE;

    /**
     * @inheritDoc
     */
    public function getId(): ?int {
        return $this->id;
    }

    /**
     * @inheritDoc
     */
    public function setId(?int $id): PriceInterface {
        $this->id = $id;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getUsd(): ?string {
        return $this->usd;
    }

    /**
     * @inheritDoc
     */
    public function setUsd(?string $usd): PriceInterface {
        $this->usd = $usd;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getEur(): ?string {
        return $this->eur;
    }

    /**
     * @inheritDoc
     */
    public function setEur(?string $eur): PriceInterface {
        $this->eur = $eur;
        return $this;
    }
}
