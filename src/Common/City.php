<?php
/**
 * City.php
 * @author Alexey Kibalnikov (alexey.kibalnikov@gmail.com)
 * @copyright (c) iWeekender 2019
 * @package DataModels\Common
 */

namespace iWeekender\DataModels\Common;

use iWeekender\DataModels\AbstractDataModelElement;
use iWeekender\Contract\DataModels\Common\CityInterface;

/**
 * Class City
 */
final class City extends AbstractDataModelElement implements CityInterface
{
    protected $propertyMapsToAssociativeArray = [
        'id',
        'countryCode',
        'name',
        'population',
        'iata',
        'lat',
        'long',
        'timezone',
        'additionalAirportCode',
        'source'
    ];

    /**
     * @var int|null
     */
    private $id;

    /**
     * @var string|null
     */
    private $countryCode;

    /**
     * @var string|null
     */
    private $name;

    /**
     * @var int|null
     */
    private $population;

    /**
     * @var string|null
     */
    private $iata;

    /**
     * @var float
     */
    private $lat = self::DEFAULT_LAT_LONG;

    /**
     * @var float
     */
    private $long = self::DEFAULT_LAT_LONG;

    /**
     * @var string|null
     */
    private $timezone = self::DEFAULT_TIMEZONE;

    /**
     * @var string|null
     */
    private $additionalAirportCode;

    /**
     * @var string|null
     */
    private $source;

    /**
     * @inheritDoc
     */
    public function getId(): ?int {
        return $this->id;
    }

    /**
     * @inheritDoc
     */
    public function setId(?int $id): CityInterface {
        $this->id = $id;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getCountryCode(): ?string {
        return $this->countryCode;
    }

    /**
     * @inheritDoc
     */
    public function setCountryCode(?string $countryCode): CityInterface {
        $this->countryCode = $countryCode;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getName(): ?string {
        return $this->name;
    }

    /**
     * @inheritDoc
     */
    public function setName(?string $name): CityInterface {
        $this->name = $name;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getPopulation(): ?int {
        return $this->population;
    }

    /**
     * @inheritDoc
     */
    public function setPopulation(?int $population): CityInterface {
        $this->population = $population;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getIata(): ?string {
        return $this->iata;
    }

    /**
     * @inheritDoc
     */
    public function setIata(?string $iata): CityInterface {
        $this->iata = $iata;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getLat(): float {
        return $this->lat;
    }

    /**
     * @inheritDoc
     */
    public function setLat(float $lat): CityInterface {
        $this->lat = $lat;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getLong(): float {
        return $this->long;
    }

    /**
     * @inheritDoc
     */
    public function setLong(float $long): CityInterface {
        $this->long = $long;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getTtimeZone(): ?string {
        return $this->timezone;
    }

    /**
     * @inheritDoc
     */
    public function setTtimeZone(?string $timezone): CityInterface {
        $this->timezone = $timezone;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getAdditionalAirportCode(): ?string {
        return $this->additionalAirportCode;
    }

    /**
     * @inheritDoc
     */
    public function setAdditionalAirportCode(?string $additionalAirportCode): CityInterface {
        $this->additionalAirportCode = $additionalAirportCode;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getSource(): ?string {
        return $this->source;
    }

    /**
     * @inheritDoc
     */
    public function setSource(?string $source): CityInterface {
        $this->source = $source;
        return $this;
    }
}
