<?php
/**
 * Country.php
 * @author Alexey Kibalnikov (alexey.kibalnikov@gmail.com)
 * @copyright (c) iWeekender 2019
 * @package DataModels\Common
 */

namespace iWeekender\DataModels\Common;

use iWeekender\DataModels\AbstractDataModelElement;
use iWeekender\Contract\DataModels\Common\CountryInterface;

/**
 * Class Country
 */
final class Country extends AbstractDataModelElement implements CountryInterface
{
    protected $propertyMapsToAssociativeArray = [
        'code',
        'name',
        'capitalCityId',
        'capitalName'
    ];

    /**
     * @var string|null
     */
    private $code;

    /**
     * @var string|null
     */
    private $name;

    /**
     * @var int|null
     */
    private $capitalCityId;

    /**
     * @var string|null
     */
    private $capitalName;

    /**
     * @inheritDoc
     */
    public function getCode(): ?string {
        return $this->code;
    }

    /**
     * @inheritDoc
     */
    public function setCode(?string $code): CountryInterface {
        $this->code = $code;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getName(): ?string {
        return $this->name;
    }

    /**
     * @inheritDoc
     */
    public function setName(?string $name): CountryInterface {
        $this->name = $name;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getCapitalCityId(): ?int {
        return $this->capitalCityId;
    }

    /**
     * @inheritDoc
     */
    public function setCapitalCityId(?int $capitalCityId): CountryInterface {
        $this->capitalCityId = $capitalCityId;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getCapitalName(): ?string {
        return $this->capitalName;
    }

    /**
     * @inheritDoc
     */
    public function setCapitalName(?string $capitalName): CountryInterface {
        $this->capitalName = $capitalName;
        return $this;
    }
}
