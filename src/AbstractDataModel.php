<?php
/**
 * AbstractDataModel.php
 * @author Alexey Kibalnikov (alexey.kibalnikov@gmail.com)
 * @copyright (c) iWeekender 2019
 * @package DataModels
 */

namespace iWeekender\DataModels;

use iWeekender\Contract\ImportExport\DataFormatEnumInterface;
use iWeekender\Contract\DataModels\DataModelInterface;
use iWeekender\Utils\ImportExport\AbstractImportExport;

/**
 * Class AbstractDataModel
 */
abstract class AbstractDataModel extends AbstractImportExport implements DataModelInterface
{
    const IMPORT_FORMAT = DataFormatEnumInterface::JSON;
    const EXPORT_FORMAT = DataFormatEnumInterface::JSON;

    /**
     * AbstractDataModel constructor.
     * @inheritDoc
     */
    public function __construct() {
        parent::__construct(self::IMPORT_FORMAT, self::EXPORT_FORMAT);
    }

    /**
     * Example:
     * 'duration'      -> 'duration'
     * 'legCollection' -> 'legs'
     *
     * @param $property
     * @return string
     */
    protected function getAssociativeArrayKey($property): string {
        return str_replace('Collection', 's', $property);
    }
}
