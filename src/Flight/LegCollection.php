<?php
/**
 * LegCollection.php
 * @author Alexey Kibalnikov (alexey.kibalnikov@gmail.com)
 * @copyright (c) iWeekender 2019
 * @package DataModels\Flight
 */

namespace iWeekender\DataModels\Flight;

use iWeekender\Contract\DataModels\Flight\LegCollectionInterface;
use iWeekender\DataModels\AbstractDataModelCollection;

/**
 * Class LegCollection
 * @property Leg[] $collection
 */
final class LegCollection extends AbstractDataModelCollection implements LegCollectionInterface
{
    protected $classOfEment = Leg::class;
}
