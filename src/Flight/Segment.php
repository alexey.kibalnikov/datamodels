<?php
/**
 * Segment.php
 * @author Alexey Kibalnikov (alexey.kibalnikov@gmail.com)
 * @copyright (c) iWeekender 2019
 * @package DataModels\Flight
 */

namespace iWeekender\DataModels\Flight;

use iWeekender\DataModels\AbstractDataModelElement;
use iWeekender\Contract\DataModels\Flight\DurationInterface;
use iWeekender\Contract\DataModels\Flight\LegCollectionInterface;
use iWeekender\Contract\DataModels\Flight\LegInterface;
use iWeekender\Contract\DataModels\Flight\SegmentInterface;

/**
 * Class Segment
 */
final class Segment extends AbstractDataModelElement implements SegmentInterface
{
    protected $propertyMapsToAssociativeArray = [
        'duration',
        'legCollection'
    ];

    protected $propertyMapsClass = [
        'duration'      => Duration::class,
        'legCollection' => LegCollection::class
    ];

    /**
     * @var DurationInterface
     */
    private $duration;

    /**
     * @var LegCollectionInterface
     */
    private $legCollection;

    /**
     * @return SegmentInterface
     */
    public function calculateDuration(): SegmentInterface {
        $flight  = 0;
        $layover = 0;
        /* @var Leg $leg */
        foreach ($this->getLegCollection() as $leg) {
            $flight  += $leg->getDuration()->getFlight();
            $layover += $leg->getDuration()->getLayover();
        }
        $this->duration->setFlight($flight);
        $this->duration->setLayover($layover);

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getDuration(): DurationInterface {
        return $this->duration;
    }

    /**
     * @inheritDoc
     */
    public function setDuration(DurationInterface $duration): SegmentInterface {
        $this->duration = $duration;
        return $this;
    }

    /**
     * @inheritDoc
     * @return LegInterface[]
     */
    public function getLegCollection(): LegCollectionInterface {
        return $this->legCollection;
    }

    /**
     * @inheritDoc
     */
    public function setLegCollection(LegCollectionInterface $legCollection): SegmentInterface {
        $this->legCollection = $legCollection;
        return $this;
    }
}
