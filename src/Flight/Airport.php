<?php
/**
 * Airport.php
 * @author Alexey Kibalnikov (alexey.kibalnikov@gmail.com)
 * @copyright (c) iWeekender 2019
 * @package DataModels\Flight
 */

namespace iWeekender\DataModels\Flight;

use iWeekender\DataModels\AbstractDataModelElement;
use iWeekender\Contract\DataModels\Flight\AirportInterface;
use Exception;

/**
 * Class Airport
 */
final class Airport extends AbstractDataModelElement implements AirportInterface
{
    protected $propertyMapsToAssociativeArray = [
        'iata',
        'city',
        'localDateTime',
        'utcDateTime'
    ];

    /**
     * @var string
     */
    private $iata = '';

    /**
     * @var string
     */
    private $city = '';

    /**
     * @var string
     */
    private $localDateTime = '';

    /**
     * @var string
     */
    private $utcDateTime = '';

    /**
     * @inheritDoc
     */
    public function getIata(): string {
        return $this->iata;
    }

    /**
     * @inheritDoc
     */
    public function setIata(string $iata): AirportInterface {
        $this->iata = $iata;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getCity(): string {
        return $this->city;
    }

    /**
     * @inheritDoc
     */
    public function setCity(string $city): AirportInterface {
        $this->city = $city;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getLocalDateTime(): string {
        return $this->localDateTime;
    }

    /**
     * @inheritDoc
     * @throws Exception
     */
    public function setLocalDateTime(string $localDateTime): AirportInterface {
        $this->localDateTime = $localDateTime;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getUtcDateTime(): string {
        return $this->utcDateTime;
    }

    /**
     * @inheritDoc
     */
    public function setUtcDateTime(string $utcDateTime): AirportInterface {
        $this->utcDateTime = $utcDateTime;
        return $this;
    }
}
