<?php
/**
 * SegmentCollection.php
 * @author Alexey Kibalnikov (alexey.kibalnikov@gmail.com)
 * @copyright (c) iWeekender 2019
 * @package DataModels\Flight
 */

namespace iWeekender\DataModels\Flight;

use iWeekender\Contract\DataModels\Flight\SegmentCollectionInterface;
use iWeekender\DataModels\AbstractDataModelCollection;

/**
 * Class SegmentCollection
 * @property Segment[] $collection
 */
final class SegmentCollection extends AbstractDataModelCollection implements SegmentCollectionInterface
{
    protected $classOfEment = Segment::class;
}
