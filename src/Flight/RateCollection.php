<?php
/**
 * RateCollection.php
 * @author Alexey Kibalnikov (alexey.kibalnikov@gmail.com)
 * @copyright (c) iWeekender 2019
 * @package DataModels\Flight
 */

namespace iWeekender\DataModels\Flight;

use iWeekender\Contract\DataModels\Flight\RateCollectionInterface;
use iWeekender\DataModels\AbstractDataModelCollection;

/**
 * Class RateCollection
 * @property Rate[] $collection
 */
final class RateCollection extends AbstractDataModelCollection implements RateCollectionInterface
{
    protected $classOfEment = Rate::class;
}
