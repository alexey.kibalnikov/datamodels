<?php
/**
 * Leg.php
 * @author Alexey Kibalnikov (alexey.kibalnikov@gmail.com)
 * @copyright (c) iWeekender 2019
 * @package DataModels\Flight
 */

namespace iWeekender\DataModels\Flight;

use iWeekender\DataModels\AbstractDataModelElement;
use iWeekender\Contract\DataModels\Flight\AirportInterface;
use iWeekender\Contract\DataModels\Flight\BaggageInterface;
use iWeekender\Contract\DataModels\Flight\DurationInterface;
use iWeekender\Contract\DataModels\Flight\LegInterface;

/**
 * Class Leg
 */
final class Leg extends AbstractDataModelElement implements LegInterface
{
    protected $propertyMapsToAssociativeArray = [
        'arrival',
        'departure',
        'flightNumber',
        'airline',
        'personalItem',
        'checkedBaggage',
        'handBaggage',
        'refundable',
        'changable',
        'duration'
    ];

    protected $propertyMapsClass = [
        'arrival'        => Airport::class,
        'departure'      => Airport::class,
        'personalItem'   => Baggage::class,
        'checkedBaggage' => Baggage::class,
        'handBaggage'    => Baggage::class,
        'duration'       => Duration::class
    ];

    /**
     * @var Airport
     */
    private $arrival;

    /**
     * @var Airport
     */
    private $departure;

    /**
     * @var string
     */
    private $flightNumber = '';

    /**
     * @var string
     */
    private $airline = '';

    /**
     * @var Baggage
     */
    private $personalItem;

    /**
     * @var Baggage
     */
    private $checkedBaggage;

    /**
     * @var Baggage
     */
    private $handBaggage;

    /**
     * @var bool
     */
    private $refundable = false;

    /**
     * @var bool
     */
    private $changable = false;

    /**
     * @var Duration
     */
    private $duration;

    /**
     * @inheritDoc
     */
    public function getArrival(): AirportInterface {
        return $this->arrival;
    }

    /**
     * @inheritDoc
     */
    public function setArrival(AirportInterface $arrival): LegInterface {
        $this->arrival = $arrival;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getDeparture(): AirportInterface {
        return $this->departure;
    }

    /**
     * @inheritDoc
     */
    public function setDeparture(AirportInterface $departure): LegInterface {
        $this->departure = $departure;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getFlightNumber(): string {
        return $this->flightNumber;
    }

    /**
     * @inheritDoc
     */
    public function setFlightNumber(string $flightNumber): LegInterface {
        $this->flightNumber = $flightNumber;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function isRefundable(): bool {
        return $this->refundable;
    }

    /**
     * @inheritDoc
     */
    public function setRefundable(bool $refundable): LegInterface {
        $this->refundable = $refundable;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function isChangable(): bool {
        return $this->changable;
    }

    /**
     * @inheritDoc
     */
    public function setChangable(bool $changable): LegInterface {
        $this->changable = $changable;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getAirline(): string {
        return $this->airline;
    }

    /**
     * @inheritDoc
     */
    public function setAirline(string $airline): LegInterface {
        $this->airline = $airline;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getPersonalItem(): BaggageInterface {
        return $this->personalItem;
    }

    /**
     * @inheritDoc
     */
    public function setPersonalItem(BaggageInterface $personalItem): LegInterface {
        $this->personalItem = $personalItem;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getCheckedBaggage(): BaggageInterface {
        return $this->checkedBaggage;
    }

    /**
     * @inheritDoc
     */
    public function setCheckedBaggage(BaggageInterface $checkedBaggage): LegInterface {
        $this->checkedBaggage = $checkedBaggage;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getHandBaggage(): BaggageInterface {
        return $this->handBaggage;
    }

    /**
     * @inheritDoc
     */
    public function setHandBaggage(BaggageInterface $handBaggage): LegInterface {
        $this->handBaggage = $handBaggage;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getDuration(): DurationInterface {
        return $this->duration;
    }

    /**
     * @inheritDoc
     */
    public function setDuration(DurationInterface $duration): LegInterface {
        $this->duration = $duration;
        return $this;
    }
}
