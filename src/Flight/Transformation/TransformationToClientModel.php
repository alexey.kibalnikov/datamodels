<?php
/**
 * TransformationToClientModel.php
 * @author Alexey Kibalnikov (alexey.kibalnikov@gmail.com)
 * @copyright (c) iWeekender 2019
 * @package DataModels\Flight\Transformation
 */

namespace iWeekender\DataModels\Flight\Transformation;

use iWeekender\DataModels\Exceptions\ExceptionMessagesInterface as EM;
use iWeekender\Contract\DataModels\DataModelVersionEnumInterface;
use iWeekender\Contract\DataModels\DataModelInterface;
use iWeekender\Contract\DataModels\Flight\BaggageInterface;
use iWeekender\Contract\DataModels\Flight\RateCollectionInterface;
use iWeekender\Contract\DataModels\Flight\RateInterface;
use iWeekender\Contract\DataModels\Flight\SegmentCollectionInterface;
use iWeekender\Contract\DataModels\Flight\SegmentInterface;
use iWeekender\Contract\DataModels\Flight\LegInterface;
use iWeekender\Exceptions\IWException;
use iWeekender\Utils\DataModelConvertation;
use iWeekender\Utils\Response\ResponseHelper;
use Exception;

/**
 * Class TransformationToClientModel
 */
class TransformationToClientModel
{
    const SUPPLIER            = 'supplier';
    const ID                  = 'id';
    const CURRENCY            = 'currency';
    const PRICE               = 'price';
    const BOOKING_TOKEN       = 'booking_token';
    const BOOKING_TOKEN_MODEL = 'bookingToken';
    const DEPARTURE_FLIGHT    = 'departure_flight';
    const RETURN_FLIGHT       = 'return_flight';
    const FLIGHT_NO           = 'flight_no';
    const AIR_LINE            = 'airline';
    const PIBAGGAGE           = 'pibaggage';
    const CBAGGAGE            = 'cbaggage';
    const BAGGAGE             = 'baggage';
    const WEIGHT              = 'weight';
    const PIECE               = 'piece';
    const WIDTH               = 'width';
    const HEIGHT              = 'height';
    const LENGTH              = 'length';
    const REFUND              = 'refund';
    const CHANGE              = 'change';
    const FLY_FROM            = 'flyFrom';
    const CITY_FROM           = 'cityFrom';
    const D_TIME              = 'dTime';
    const FLY_TO              = 'flyTo';
    const CITY_TO             = 'cityTo';
    const A_TIME              = 'aTime';
    const DURATION            = 'duration';

    /**
     * @var string
     */
    private $dataModelVersion;

    /**
     * @var bool
     */
    private $isUseWrapper;

    /**
     * @var string
     */
    private $requestID;

    public function __construct(string $dataModelVersion, bool $isUseWrapper, string $requestID) {
        $this->dataModelVersion = $dataModelVersion;
        $this->isUseWrapper     = $isUseWrapper;
        $this->requestID        = $requestID;
    }

    /**
     * @param DataModelInterface $model
     * @return array
     * @throws Exception
     * @throws IWException
     */
    public function run(DataModelInterface $model): array {
        if ($model instanceof RateCollectionInterface) {
            return $this->getRateCollectionToClientModel($model);
        } elseif ($model instanceof RateInterface) {
            return $this->getRateToClientModel($model);
        } elseif ($model instanceof SegmentCollectionInterface) {
            return $this->getSegmentCollectionToClientModel($model);
        } else {
            throw new IWException(EM::MES_TRANSFORMATION_NOT_SUPPORTED);
        }
    }

    /**
     * @param RateCollectionInterface $rateCollection
     * @return array
     * @throws Exception
     */
    private function getRateCollectionToClientModel(RateCollectionInterface $rateCollection): array {
        $result = [];
        foreach ($rateCollection as $rate) {
            $result[] = $this->getRateToClientModel($rate);
        }
        return $result;
    }

    /**
     * @param RateInterface $rate
     * @return array
     * @throws Exception
     */
    private function getRateToClientModel(RateInterface $rate): array {
        if ($this->dataModelVersion === DataModelVersionEnumInterface::OLD_VERSION) {
            return $this->rateTransformation($rate);
        } else {
            return $this->rate($rate);
        }
    }

    /**
     * @param SegmentCollectionInterface $segmentCollection
     * @return array
     * @throws Exception
     */
    private function getSegmentCollectionToClientModel(SegmentCollectionInterface $segmentCollection): array {
        if ($this->dataModelVersion === DataModelVersionEnumInterface::OLD_VERSION) {
            $departureFlight = [];
            if (!empty($segmentCollection[0])) {
                $departureFlight = $this->segmentTransformation($segmentCollection[0]);
            }

            $returnFlight = [];
            if (!empty($segmentCollection[1])) {
                $returnFlight = $this->segmentTransformation($segmentCollection[1]);
            }

            return [
                self::DEPARTURE_FLIGHT => $departureFlight,
                self::RETURN_FLIGHT    => $returnFlight
            ];
        } else {
            return $segmentCollection->getAssociativeArray();
        }
    }

    /**
     * @param RateInterface $rate
     * @return string
     * @throws Exception
     */
    private function getSerpId(RateInterface $rate): string {
        return ResponseHelper::getSerpId($this->requestID, $rate->getSupplier(), $rate->getId());
    }

    /**
     * @param RateInterface $rate
     * @return array
     * @throws Exception
     */
    private function rate(RateInterface $rate): array {
        $result = $rate->getAssociativeArray();

        if ($this->isUseWrapper) {
            unset($result[self::BOOKING_TOKEN_MODEL]);
            $result[self::ID] = $this->getSerpId($rate);
        }

        return $result;
    }

    /**
     * @param RateInterface $rate
     * @return array
     * @throws Exception
     */
    private function rateTransformation(RateInterface $rate): array {
        $departureFlight = [];
        if (!empty($rate->getSegmentCollection()[0])) {
            $departureFlight = $this->segmentTransformation($rate->getSegmentCollection()[0]);
        }

        $returnFlight = [];
        if (!empty($rate->getSegmentCollection()[1])) {
            $returnFlight = $this->segmentTransformation($rate->getSegmentCollection()[1]);
        }

        $result = [
            self::SUPPLIER         => $rate->getSupplier(),
            self::ID               => $rate->getId(),
            self::CURRENCY         => $rate->getCurrency(),
            self::PRICE            => $rate->getPrice(),
            self::BOOKING_TOKEN    => $rate->getBookingToken(),
            self::DEPARTURE_FLIGHT => $departureFlight,
            self::RETURN_FLIGHT    => $returnFlight
        ];

        if ($this->isUseWrapper) {
            unset($result[self::BOOKING_TOKEN]);
            $result[self::ID] = $this->getSerpId($rate);
        }

        return $result;
    }

    private function segmentTransformation(SegmentInterface $segment): array {
        $result = [];
        foreach ($segment->getLegCollection() as $leg) {
            $result[] = $this->legTransformation($leg);
        }
        return $result;
    }

    private function legTransformation(LegInterface $leg): array {
        $arrival   = $leg->getArrival();
        $departure = $leg->getDeparture();
        $duration  = $leg->getDuration();

        return [
            self::FLIGHT_NO => $leg->getFlightNumber(),
            self::AIR_LINE  => $leg->getAirline(),
            self::PIBAGGAGE => $this->baggageTransformation($leg->getPersonalItem()),
            self::CBAGGAGE  => $this->baggageTransformation($leg->getHandBaggage()),
            self::BAGGAGE   => $this->baggageTransformation($leg->getCheckedBaggage()),
            self::REFUND    => $leg->isRefundable(),
            self::CHANGE    => $leg->isChangable(),
            self::FLY_FROM  => $departure->getIata(),
            self::CITY_FROM => $departure->getCity(),
            self::D_TIME    => (int)DataModelConvertation::iwDateTimeFormatToTimestamp($departure->getLocalDateTime()),
            self::FLY_TO    => $arrival->getIata(),
            self::CITY_TO   => $arrival->getCity(),
            self::A_TIME    => (int)DataModelConvertation::iwDateTimeFormatToTimestamp($arrival->getLocalDateTime()),
            self::DURATION  => $duration->getFlight()
        ];
    }

    private function baggageTransformation(BaggageInterface $baggage): array {
        return [
            self::WEIGHT => $baggage->getWeight(),
            self::PIECE  => $baggage->getPiece(),
            self::WIDTH  => $baggage->getWidth(),
            self::HEIGHT => $baggage->getHeight(),
            self::LENGTH => $baggage->getLength()
        ];
    }
}
