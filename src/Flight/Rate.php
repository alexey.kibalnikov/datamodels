<?php
/**
 * Rate.php
 * @author Alexey Kibalnikov (alexey.kibalnikov@gmail.com)
 * @copyright (c) iWeekender 2019
 * @package DataModels\Flight
 */

namespace iWeekender\DataModels\Flight;

use iWeekender\DataModels\AbstractDataModelElement;
use iWeekender\Contract\DataModels\Flight\SegmentCollectionInterface;
use iWeekender\Contract\DataModels\Flight\SegmentInterface;
use iWeekender\Contract\DataModels\Flight\RateInterface;

/**
 * Class Rate
 */
class Rate extends AbstractDataModelElement implements RateInterface
{
    protected $propertyMapsToAssociativeArray = [
        'supplier',
        'id',
        'currency',
        'price',
        'bookingToken',
        'segmentCollection'
    ];

    protected $propertyMapsClass = [
        'segmentCollection' => SegmentCollection::class
    ];

    /**
     * @var string
     */
    private $supplier = '';

    /**
     * @var string
     */
    private $id = '';

    /**
     * @var string
     */
    private $currency = '';

    /**
     * @var float
     */
    private $price = 0;

    /**
     * @var string
     */
    private $bookingToken = '';

    /**
     * @var SegmentCollection
     */
    private $segmentCollection;

    /**
     * @inheritDoc
     */
    public function getSupplier(): string {
        return $this->supplier;
    }

    /**
     * @inheritDoc
     */
    public function setSupplier(string $supplier): RateInterface {
        $this->supplier = $supplier;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getId(): string {
        return $this->id;
    }

    /**
     * @inheritDoc
     */
    public function setId(string $id): RateInterface {
        $this->id = $id;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getCurrency(): string {
        return $this->currency;
    }

    /**
     * @inheritDoc
     */
    public function setCurrency(string $currency): RateInterface {
        $this->currency = $currency;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getPrice(): float {
        return $this->price;
    }

    /**
     * @inheritDoc
     */
    public function setPrice(float $price): RateInterface {
        $this->price = $price;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getBookingToken(): string {
        return $this->bookingToken;
    }

    /**
     * @inheritDoc
     */
    public function setBookingToken(string $bookingToken): RateInterface {
        $this->bookingToken = $bookingToken;
        return $this;
    }

    /**
     * @inheritDoc
     * @return SegmentInterface[]
     */
    public function getSegmentCollection(): SegmentCollectionInterface {
        return $this->segmentCollection;
    }

    /**
     * @inheritDoc
     */
    public function setSegmentCollection(SegmentCollectionInterface $segmentCollection): RateInterface {
        $this->segmentCollection = $segmentCollection;
        return $this;
    }
}
