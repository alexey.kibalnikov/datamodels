<?php
/**
 * Duration.php
 * @author Alexey Kibalnikov (alexey.kibalnikov@gmail.com)
 * @copyright (c) iWeekender 2019
 * @package DataModels\Flight
 */

namespace iWeekender\DataModels\Flight;

use iWeekender\DataModels\AbstractDataModelElement;
use iWeekender\Contract\DataModels\Flight\DurationInterface;

/**
 * Class Duration
 */
final class Duration extends AbstractDataModelElement implements DurationInterface
{
    protected $propertyMapsToAssociativeArray = [
        'flight',
        'layover'
    ];

    /**
     * @var int
     */
    private $flight = 0;

    /**
     * @var int
     */
    private $layover = 0;

    /**
     * @inheritDoc
     */
    public function getFlight(): int {
        return $this->flight;
    }

    /**
     * @inheritDoc
     */
    public function setFlight(int $flight): DurationInterface {
        $this->flight = $flight;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getLayover(): int {
        return $this->layover;
    }

    /**
     * @inheritDoc
     */
    public function setLayover(int $layover): DurationInterface {
        $this->layover = $layover;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getTotal(): int {
        return $this->flight + $this->layover;
    }
}
