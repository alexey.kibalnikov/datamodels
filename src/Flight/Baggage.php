<?php
/**
 * Baggage.php
 * @author Alexey Kibalnikov (alexey.kibalnikov@gmail.com)
 * @copyright (c) iWeekender 2019
 * @package DataModels\Flight
 */

namespace iWeekender\DataModels\Flight;

use iWeekender\DataModels\AbstractDataModelElement;
use iWeekender\Contract\DataModels\Flight\BaggageInterface;

/**
 * Class Baggage
 */
final class Baggage extends AbstractDataModelElement implements BaggageInterface
{
    protected $propertyMapsToAssociativeArray = [
        'weight',
        'piece',
        'width',
        'height',
        'length'
    ];

    /**
     * @var int
     */
    private $weight = 0;

    /**
     * @var int
     */
    private $piece = 0;

    /**
     * @var int
     */
    private $width = 0;

    /**
     * @var int
     */
    private $height = 0;

    /**
     * @var int
     */
    private $length = 0;

    /**
     * @inheritDoc
     */
    public function getWeight(): int {
        return $this->weight;
    }

    /**
     * @inheritDoc
     */
    public function setWeight(int $weight): BaggageInterface {
        $this->weight = $weight;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getPiece(): int {
        return $this->piece;
    }

    /**
     * @inheritDoc
     */
    public function setPiece(int $piece): BaggageInterface {
        $this->piece = $piece;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getWidth(): int {
        return $this->width;
    }

    /**
     * @inheritDoc
     */
    public function setWidth(int $width): BaggageInterface {
        $this->width = $width;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getHeight(): int {
        return $this->height;
    }

    /**
     * @inheritDoc
     */
    public function setHeight(int $height): BaggageInterface {
        $this->height = $height;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getLength(): int {
        return $this->length;
    }

    /**
     * @inheritDoc
     */
    public function setLength(int $length): BaggageInterface {
        $this->length = $length;
        return $this;
    }
}
