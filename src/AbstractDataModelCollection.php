<?php
/**
 * AbstractDataModelCollection.php
 * @author Alexey Kibalnikov (alexey.kibalnikov@gmail.com)
 * @copyright (c) iWeekender 2019
 * @package DataModels
 */

namespace iWeekender\DataModels;

use iWeekender\DataModels\Exceptions\ExceptionMessagesInterface as EM;
use iWeekender\Contract\DataModels\DataModelInterface;
use iWeekender\Contract\DataModels\DataModelCollectionInterface;
use iWeekender\Exceptions\IWException;
use ArrayIterator;
use Traversable;

/**
 * Class AbstractDataModelCollection
 */
abstract class AbstractDataModelCollection extends AbstractDataModel implements DataModelCollectionInterface
{
    protected $classOfEment = null;
    protected $collection = [];

    /**
     * @inheritDoc
     * @throws IWException
     */
    public function __construct() {
        parent::__construct();
        $this->validateConfiguration();
    }

    /**
     * @throws IWException
     */
    private function validateConfiguration(): void {
        $className = $this->classOfEment;
        if (is_null($className)) {
            throw new IWException(EM::MES_DATA_MODEL_COLLECTION_MAP_CLASS);
        } else {
            $interfaces = class_implements($className);
            if (!$interfaces || !in_array(DataModelInterface::class, $interfaces)) {
                throw new IWException(
                    sprintf(
                        EM::MES_CLASS_NOT_IMPLEMENT_INTERFACE,
                        $className,
                        DataModelInterface::class
                    )
                );
            }
        }
    }

    /**
     * @inheritDoc
     */
    public function getAssociativeArray(): array {
        $result = [];
        foreach ($this->collection as $item) {
            $result[] = $item->getAssociativeArray();
        }
        return $result;
    }

    /**
     * @inheritDoc
     */
    public function loadToObject(array $associativeArray): void {
        $this->collection = [];
        $className = $this->classOfEment;
        foreach ($associativeArray as $item) {
            $object = new $className();
            /** @var DataModelInterface $object */
            $object->loadToObject($item);
            $this->collection[] = $object;
        }
    }

    /**
     * @inheritDoc
     */
    public function merge(DataModelCollectionInterface $model): DataModelCollectionInterface {
        $this->collection = array_merge($this->collection, $model->getCollection());
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function offsetExists($offset) {
        return isset($this->collection[$offset]);
    }

    /**
     * @inheritDoc
     */
    public function offsetGet($offset) {
        return isset($this->collection[$offset]) ? $this->collection[$offset] : null;
    }

    /**
     * @inheritDoc
     */
    public function offsetSet($offset, $value) {
        if (is_null($offset)) {
            $this->collection[] = $value;
        } else {
            $this->collection[$offset] = $value;
        }
    }

    /**
     * @inheritDoc
     */
    public function offsetUnset($offset) {
        unset($this->collection[$offset]);
    }

    /**
     * @inheritDoc
     */
    public function count(): int {
        return count($this->collection);
    }

    /**
     * @inheritDoc
     */
    public function getIterator(): Traversable {
        return new ArrayIterator($this->collection);
    }

    /**
     * @inheritDoc
     */
    public function getCollection(): array {
        return $this->collection;
    }
}
