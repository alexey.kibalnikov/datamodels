<?php
/**
 * Performance.php
 * @author Alexey Kibalnikov (alexey.kibalnikov@gmail.com)
 * @copyright (c) iWeekender 2019
 * @package DataModels\Event
 */

namespace iWeekender\DataModels\Event;

use iWeekender\Contract\DataModels\Event\PerformanceInterface;
use iWeekender\Contract\DataModels\Event\PerformerInterface;
use iWeekender\DataModels\AbstractDataModelElement;

/**
 * Class Performance
 */
final class Performance extends AbstractDataModelElement implements PerformanceInterface
{
    protected $propertyMapsToAssociativeArray = [
        'id',
        'eventId',
        'performer',
        'primary'
    ];

    protected $propertyMapsClass = [
        '$performer' => Performer::class
    ];

    /**
     * @var int|null
     */
    private $id;

    /**
     * @var int|null
     */
    private $eventId;

    /**
     * @var PerformerInterface|null
     */
    private $performer;

    /**
     * @var bool
     */
    private $primary;

    /**
     * @inheritDoc
     */
    public function getId(): ?int {
        return $this->id;
    }

    /**
     * @inheritDoc
     */
    public function setId(?int $id): PerformanceInterface {
        $this->id = $id;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getEventId(): ?int {
        return $this->eventId;
    }

    /**
     * @inheritDoc
     */
    public function setEventId(?int $eventId): PerformanceInterface {
        $this->eventId = $eventId;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getPerformer(): ?PerformerInterface {
        return $this->performer;
    }

    /**
     * @inheritDoc
     */
    public function setPerformer(?PerformerInterface $performer): PerformanceInterface {
        $this->performer = $performer;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function isPrimary(): bool {
        return $this->primary;
    }

    /**
     * @inheritDoc
     */
    public function setPrimary(bool $primary): PerformanceInterface {
        $this->primary = $primary;
        return $this;
    }
}
