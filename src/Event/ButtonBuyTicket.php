<?php
/**
 * ButtonBuyTicket.php
 * @author Alexey Kibalnikov (alexey.kibalnikov@gmail.com)
 * @copyright (c) iWeekender 2019
 * @package DataModels\Event
 */

namespace iWeekender\DataModels\Event;

use iWeekender\DataModels\AbstractDataModelElement;
use iWeekender\Contract\DataModels\Event\ButtonBuyTicketInterface;

/**
 * Class ButtonBuyTicket
 */
final class ButtonBuyTicket extends AbstractDataModelElement implements ButtonBuyTicketInterface
{
    protected $propertyMapsToAssociativeArray = [
        'eventId',
        'color',
        'url',
        'description',
        'recommendation',
        'callToAction'
    ];

    /**
     * @var int|null
     */
    private $eventId;

    /**
     * @var string|null
     */
    private $color;

    /**
     * @var string|null
     */
    private $url;

    /**
     * @var string|null
     */
    private $description;

    /**
     * @var string|null
     */
    private $recommendation;

    /**
     * @var string|null
     */
    private $callToAction;

    /**
     * @inheritDoc
     */
    public function getColor(): ?string {
        return $this->color;
    }

    /**
     * @inheritDoc
     */
    public function getEventId(): ?int {
        return $this->eventId;
    }

    /**
     * @inheritDoc
     */
    public function setEventId(?int $eventId): ButtonBuyTicketInterface {
        $this->eventId = $eventId;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function setColor(?string $color): ButtonBuyTicketInterface {
        $this->color = $color;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getUrl(): ?string {
        return $this->url;
    }

    /**
     * @inheritDoc
     */
    public function setUrl(?string $url): ButtonBuyTicketInterface {
        $this->url = $url;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getDescription(): ?string {
        return $this->description;
    }

    /**
     * @inheritDoc
     */
    public function setDescription(?string $description): ButtonBuyTicketInterface {
        $this->description = $description;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getRecommendation(): ?string {
        return $this->recommendation;
    }

    /**
     * @inheritDoc
     */
    public function setRecommendation(?string $recommendation): ButtonBuyTicketInterface {
        $this->recommendation = $recommendation;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getCallToAction(): ?string {
        return $this->callToAction;
    }

    /**
     * @inheritDoc
     */
    public function setCallToAction(?string $callToAction): ButtonBuyTicketInterface {
        $this->callToAction = $callToAction;
        return $this;
    }
}
