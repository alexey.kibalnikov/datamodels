<?php
/**
 * Group.php
 * @author Alexey Kibalnikov (alexey.kibalnikov@gmail.com)
 * @copyright (c) iWeekender 2019
 * @package DataModels\Event
 */

namespace iWeekender\DataModels\Event;

use iWeekender\DataModels\AbstractDataModelElement;
use iWeekender\Contract\DataModels\Event\GroupInterface;

/**
 * Class Group
 */
final class Group extends AbstractDataModelElement implements GroupInterface
{
    protected $propertyMapsToAssociativeArray = [
        'id',
        'name'
    ];

    /**
     * @var int|null
     */
    private $id;

    /**
     * @var string|null
     */
    private $name;

    /**
     * @inheritDoc
     */
    public function getId(): ?int {
        return $this->id;
    }

    /**
     * @inheritDoc
     */
    public function setId(?int $id): GroupInterface {
        $this->id = $id;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getName(): ?string {
        return $this->name;
    }

    /**
     * @inheritDoc
     */
    public function setName(?string $name): GroupInterface {
        $this->name = $name;
        return $this;
    }
}
