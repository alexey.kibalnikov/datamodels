<?php
/**
 * EventCollection.php
 * @author Alexey Kibalnikov (alexey.kibalnikov@gmail.com)
 * @copyright (c) iWeekender 2019
 * @package DataModels\Event
 */

namespace iWeekender\DataModels\Event;

use iWeekender\Contract\DataModels\Event\EventCollectionInterface;
use iWeekender\DataModels\AbstractDataModelCollection;

/**
 * Class EventCollection
 * @property Event[] $collection
 */
final class EventCollection extends AbstractDataModelCollection implements EventCollectionInterface
{
    protected $classOfEment = Event::class;
}
