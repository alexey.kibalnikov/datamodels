<?php
/**
 * EventTime.php
 * @author Alexey Kibalnikov (alexey.kibalnikov@gmail.com)
 * @copyright (c) iWeekender 2019
 * @package DataModels\Event
 */

namespace iWeekender\DataModels\Event;

use iWeekender\DataModels\AbstractDataModelElement;
use iWeekender\Contract\DataModels\Event\EventTimeInterface;

/**
 * Class EventTime
 */
final class EventTime extends AbstractDataModelElement implements EventTimeInterface
{
    protected $propertyMapsToAssociativeArray = [
        'eventId',
        'startLocal',
        'endLocal',
        'startUtc',
        'endUtc'
    ];

    /**
     * @var int|null
     */
    private $eventId;

    /**
     * @var string|null
     */
    private $startLocal;

    /**
     * @var string|null
     */
    private $endLocal;

    /**
     * @var string|null
     */
    private $startUtc;

    /**
     * @var string|null
     */
    private $endUtc;

    /**
     * @inheritDoc
     */
    public function getEventId(): ?int {
        return $this->eventId;
    }

    /**
     * @inheritDoc
     */
    public function setEventId(?int $eventId): EventTimeInterface {
        $this->eventId = $eventId;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getStartLocal(): ?string {
        return $this->startLocal;
    }

    /**
     * @inheritDoc
     */
    public function setStartLocal(?string $startLocal): EventTimeInterface {
        $this->startLocal = $startLocal;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getEndLocal(): ?string {
        return $this->endLocal;
    }

    /**
     * @inheritDoc
     */
    public function setEndLocal(?string $endLocal): EventTimeInterface {
        $this->endLocal = $endLocal;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getStartUtc(): ?string {
        return $this->startUtc;
    }

    /**
     * @inheritDoc
     */
    public function setStartUtc(?string $startUtc): EventTimeInterface {
        $this->startUtc = $startUtc;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getEndUtc(): ?string {
        return $this->endUtc;
    }

    /**
     * @inheritDoc
     */
    public function setEndUtc(?string $endUtc): EventTimeInterface {
        $this->endUtc = $endUtc;
        return $this;
    }
}
