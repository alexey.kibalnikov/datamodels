<?php
/**
 * Image.php
 * @author Alexey Kibalnikov (alexey.kibalnikov@gmail.com)
 * @copyright (c) iWeekender 2019
 * @package DataModels\Event
 */

namespace iWeekender\DataModels\Event;

use iWeekender\Contract\DataModels\Event\ImageInterface;
use iWeekender\DataModels\AbstractDataModelElement;

/**
 * Class Image
 */
final class Image extends AbstractDataModelElement implements ImageInterface
{
    protected $propertyMapsToAssociativeArray = [
        'id',
        'eventId',
        'ratio',
        'url',
        'order',
        'height',
        'width'
    ];

    /**
     * @var int|null
     */
    private $id;

    /**
     * @var int|null
     */
    private $eventId;

    /**
     * @var string|null
     */
    private $ratio = '';

    /**
     * @var string|null
     */
    private $url;

    /**
     * @var int|null
     */
    private $order = 0;

    /**
     * @var int|null
     */
    private $height = 0;

    /**
     * @var int|null
     */
    private $width = 0;

    /**
     * @inheritDoc
     */
    public function getId(): ?int {
        return $this->id;
    }

    /**
     * @inheritDoc
     */
    public function setId(?int $id): ImageInterface {
        $this->id = $id;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getEventId(): ?int {
        return $this->eventId;
    }

    /**
     * @inheritDoc
     */
    public function setEventId(?int $eventId): ImageInterface {
        $this->eventId = $eventId;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getRatio(): ?string {
        return $this->ratio;
    }

    /**
     * @inheritDoc
     */
    public function setRatio(?string $ratio): ImageInterface {
        $this->ratio = $ratio;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getUrl(): ?string {
        return $this->url;
    }

    /**
     * @inheritDoc
     */
    public function setUrl(?string $url): ImageInterface {
        $this->url = $url;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getOrder(): ?int {
        return $this->order;
    }

    /**
     * @inheritDoc
     */
    public function setOrder(?int $order): ImageInterface {
        $this->order = $order;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getHeight(): ?int {
        return $this->height;
    }

    /**
     * @inheritDoc
     */
    public function setHeight(?int $height): ImageInterface {
        $this->height = $height;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getWidth(): ?int {
        return $this->width;
    }

    /**
     * @inheritDoc
     */
    public function setWidth(?int $width): ImageInterface {
        $this->width = $width;
        return $this;
    }
}
