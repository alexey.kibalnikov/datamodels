<?php
/**
 * Organizer.php
 * @author Alexey Kibalnikov (alexey.kibalnikov@gmail.com)
 * @copyright (c) iWeekender 2019
 * @package DataModels\Event
 */

namespace iWeekender\DataModels\Event;

use iWeekender\Contract\DataModels\Event\OrganizerInterface;
use iWeekender\DataModels\AbstractDataModelElement;

/**
 * Class Organizer
 */
final class Organizer extends AbstractDataModelElement implements OrganizerInterface
{
    protected $propertyMapsToAssociativeArray = [
        'id',
        'name',
        'description',
        'officialWebsite'
    ];

    /**
     * @var int|null
     */
    private $id;

    /**
     * @var string|null
     */
    private $name = '';

    /**
     * @var string|null
     */
    private $description;

    /**
     * @var string|null
     */
    private $officialWebsite;

    /**
     * @inheritDoc
     */
    public function getId(): ?int {
        return $this->id;
    }

    /**
     * @inheritDoc
     */
    public function setId(?int $id): OrganizerInterface {
        $this->id = $id;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getName(): ?string {
        return $this->name;
    }

    /**
     * @inheritDoc
     */
    public function setName(?string $name): OrganizerInterface {
        $this->name = $name;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getDescription(): ?string {
        return $this->description;
    }

    /**
     * @inheritDoc
     */
    public function setDescription(?string $description): OrganizerInterface {
        $this->description = $description;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getOfficialWebsite(): ?string {
        return $this->officialWebsite;
    }

    /**
     * @inheritDoc
     */
    public function setOfficialWebsite(?string $officialWebsite): OrganizerInterface {
        $this->officialWebsite = $officialWebsite;
        return $this;
    }
}
