<?php
/**
 * Performer.php
 * @author Alexey Kibalnikov (alexey.kibalnikov@gmail.com)
 * @copyright (c) iWeekender 2019
 * @package DataModels\Event
 */

namespace iWeekender\DataModels\Event;

use iWeekender\Contract\DataModels\Event\PerformerInterface;
use iWeekender\DataModels\AbstractDataModelElement;

/**
 * Class Performer
 */
final class Performer extends AbstractDataModelElement implements PerformerInterface
{
    protected $propertyMapsToAssociativeArray = [
        'id',
        'name',
        'officialWebsite',
        'primary',
        'src'
    ];

    /**
     * @var int|null
     */
    private $id;

    /**
     * @var string|null
     */
    private $name;

    /**
     * @var string|null
     */
    private $officialWebsite;


    /**
     * @var string|null
     */
    private $source;

    /**
     * @inheritDoc
     */
    public function getId(): ?int {
        return $this->id;
    }

    /**
     * @inheritDoc
     */
    public function setId(?int $id): PerformerInterface {
        $this->id = $id;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getName(): ?string {
        return $this->name;
    }

    /**
     * @inheritDoc
     */
    public function setName(?string $name): PerformerInterface {
        $this->name = $name;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getOfficialWebsite(): ?string {
        return $this->officialWebsite;
    }

    /**
     * @inheritDoc
     */
    public function setOfficialWebsite(?string $officialWebsite): PerformerInterface {
        $this->officialWebsite = $officialWebsite;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getSource(): ?string {
        return $this->source;
    }

    /**
     * @inheritDoc
     */
    public function setSource(?string $source): PerformerInterface {
        $this->source = $source;
        return $this;
    }
}
