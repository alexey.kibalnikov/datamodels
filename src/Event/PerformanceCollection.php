<?php
/**
 * PerformanceCollection.php
 * @author Alexey Kibalnikov (alexey.kibalnikov@gmail.com)
 * @copyright (c) iWeekender 2019
 * @package DataModels\Event
 */

namespace iWeekender\DataModels\Event;

use iWeekender\Contract\DataModels\Event\PerformanceCollectionInterface;
use iWeekender\Contract\DataModels\Event\PerformanceInterface;
use iWeekender\DataModels\AbstractDataModelCollection;

/**
 * Class PerformanceCollection
 * @property Performance[] $collection
 */
final class PerformanceCollection extends AbstractDataModelCollection implements PerformanceCollectionInterface
{
    protected $classOfEment = Performer::class;

    /**
     * @var int|null
     */
    private $eventId;

    /**
     * @inheritDoc
     */
    public function getEventId(): ?int {
        return $this->eventId;
    }

    /**
     * @inheritDoc
     */
    public function setEventId(?int $eventId): PerformanceCollectionInterface {
        $this->eventId = $eventId;
        /** @var PerformanceInterface $performance */
        foreach ($this as $performance) {
            $performance->setEventId($eventId);
        }
        return $this;
    }
}
