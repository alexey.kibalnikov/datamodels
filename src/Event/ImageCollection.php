<?php
/**
 * ImageCollection.php
 * @author Alexey Kibalnikov (alexey.kibalnikov@gmail.com)
 * @copyright (c) iWeekender 2019
 * @package DataModels\Event
 */

namespace iWeekender\DataModels\Event;

use iWeekender\Contract\DataModels\Event\ImageCollectionInterface;
use iWeekender\Contract\DataModels\Event\ImageInterface;
use iWeekender\DataModels\AbstractDataModelCollection;

/**
 * Class ImageCollection
 * @property Image[] $collection
 */
final class ImageCollection extends AbstractDataModelCollection implements ImageCollectionInterface
{
    protected $classOfEment = Image::class;

    /**
     * @var int|null
     */
    private $eventId;

    /**
     * @inheritDoc
     */
    public function getEventId(): ?int {
        return $this->eventId;
    }

    /**
     * @inheritDoc
     */
    public function setEventId(?int $eventId): ImageCollectionInterface {
        $this->eventId = $eventId;
        /** @var ImageInterface $image */
        foreach ($this as $image) {
            $image->setEventId($eventId);
        }
        return $this;
    }
}
