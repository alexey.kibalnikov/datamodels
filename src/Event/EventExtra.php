<?php
/**
 * EventExtra.php
 * @author Alexey Kibalnikov (alexey.kibalnikov@gmail.com)
 * @copyright (c) iWeekender 2019
 * @package DataModels\Event
 */

namespace iWeekender\DataModels\Event;

use iWeekender\Contract\DataModels\Event\ButtonBuyTicketInterface;
use iWeekender\Contract\DataModels\Event\EventExtraInterface;
use iWeekender\DataModels\AbstractDataModelElement;

/**
 * Class EventExtra
 */
final class EventExtra extends AbstractDataModelElement implements EventExtraInterface
{
    protected $propertyMapsToAssociativeArray = [
        'eventId',
        'description',
        'information',
        'organizerNotice',
        'buttonBuyTicket'
    ];

    protected $propertyMapsClass = [
        'buttonBuyTicket' => ButtonBuyTicket::class
    ];

    /**
     * @var int|null
     */
    private $eventId;

    /**
     * @var string|null
     */
    private $description;

    /**
     * @var string|null
     */
    private $information;

    /**
     * @var string|null
     */
    private $organizerNotice;

    /**
     * @var ButtonBuyTicket
     */
    private $buttonBuyTicket;

    /**
     * @inheritDoc
     */
    public function getEventId(): ?int {
        return $this->eventId;
    }

    /**
     * @inheritDoc
     */
    public function setEventId(?int $eventId): EventExtraInterface {
        $this->eventId = $eventId;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getDescription(): ?string {
        return $this->description;
    }

    /**
     * @inheritDoc
     */
    public function setDescription(?string $description): EventExtraInterface {
        $this->description = $description;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getInformation(): ?string {
        return $this->information;
    }

    /**
     * @inheritDoc
     */
    public function setInformation(?string $information): EventExtraInterface {
        $this->information = $information;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getOrganizerNotice(): ?string {
        return $this->organizerNotice;
    }

    /**
     * @inheritDoc
     */
    public function setOrganizerNotice(?string $organizerNotice): EventExtraInterface {
        $this->organizerNotice = $organizerNotice;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getButtonBuyTicket(): ButtonBuyTicketInterface {
        return $this->buttonBuyTicket;
    }

    /**
     * @inheritDoc
     */
    public function setButtonBuyTicket(ButtonBuyTicketInterface $buttonBuyTicket): EventExtraInterface {
        $this->buttonBuyTicket = $buttonBuyTicket;
        return $this;
    }
}
