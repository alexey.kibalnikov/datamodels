<?php
/**
 * TransformationToClientModel.php
 * @author Alexey Kibalnikov (alexey.kibalnikov@gmail.com)
 * @copyright (c) iWeekender 2019
 * @package DataModels\Event\Transformation
 */

namespace iWeekender\DataModels\Event\Transformation;

use iWeekender\Contract\DataModels\Common\PriceInterface;
use iWeekender\Contract\DataModels\Event\EventExtraInterface;
use iWeekender\Contract\DataModels\Common\CityInterface;
use iWeekender\Contract\DataModels\Common\CountryInterface;
use iWeekender\Contract\DataModels\Event\EventTimeInterface;
use iWeekender\Contract\DataModels\Event\GroupInterface;
use iWeekender\Contract\DataModels\Event\ImageCollectionInterface;
use iWeekender\Contract\DataModels\Event\ClassificationInterface;
use iWeekender\Contract\DataModels\Event\ImageInterface;
use iWeekender\Contract\DataModels\Event\OrganizerInterface;
use iWeekender\Contract\DataModels\Event\PerformanceCollectionInterface;
use iWeekender\Contract\DataModels\Event\PerformanceInterface;
use iWeekender\Contract\DataModels\Event\VenueInterface;
use iWeekender\Contract\DataModels\Event\EventCollectionInterface;
use iWeekender\Contract\DataModels\Event\EventInterface;
use iWeekender\Contract\DataModels\DataModelInterface;
use iWeekender\DataModels\Exceptions\ExceptionMessagesInterface as EM;
use iWeekender\Exceptions\IWException;
use Exception;

/**
 * Class TransformationToClientModel
 */
class TransformationToClientModel
{
    const ID                = 'id';
    const CODE              = 'code';
    const NUM               = 'num';
    const NAME              = 'name';
    const EVENT_TIME        = 'event_time';
    const START_TIME        = 'start_time';
    const END_TIME          = 'end_time';
    const LOCAL             = 'local';
    const GROUP             = 'group';
    const CLASSIFICATION    = 'classification';
    const SLUG              = 'slug';
    const SEARCH_LINE       = 'search_line';
    const VENUE             = 'venue';
    const ADDRESS           = 'address';
    const LAT               = 'lat';
    const LONG              = 'long';
    const COUNTRY           = 'country';
    const CITY              = 'city';
    const IMAGES            = 'images';
    const RATIO             = 'ratio';
    const ORDER             = 'order';
    const URL               = 'url';
    const HEIGHT            = 'height';
    const WIDTH             = 'width';
    const ORGANIZER         = 'organizer';
    const EXTRA             = 'extra';
    const DESCRIPTION       = 'description';
    const INFORMRMATION     = 'information';
    const ORGANIZER_NOTICE  = 'organizer_notice';
    const BUTTON_BUY_TICKET = 'button_buy_ticket';
    const COLOR             = 'color';
    const RECOMMENDATION    = 'recommendation';
    const CALL_TO_ACTION    = 'call_to_action';
    const TICKET_URL        = 'ticket_url';
    const PERFORMERS        = 'performers';
    const OFFICIAL_WEBSITE  = 'official_website';
    const PRIMARY           = 'primary';
    const PRICE             = 'price';
    const USD               = 'USD';
    const EUR               = 'EUR';

    /**
     * @param DataModelInterface $model
     * @return array
     * @throws Exception
     * @throws IWException
     */
    public function run(DataModelInterface $model): array {
        if ($model instanceof EventCollectionInterface) {
            return $this->getEventCollectionInterfaceToClientModel($model);
        } elseif ($model instanceof EventInterface) {
            return $this->getEventToClientModel($model);
        } else {
            throw new IWException(EM::MES_TRANSFORMATION_NOT_SUPPORTED);
        }
    }

    /**
     * @param $eventCollection
     * @return array
     * @throws Exception
     */
    private function getEventCollectionInterfaceToClientModel(EventCollectionInterface $eventCollection): array {
        $result = [];
        foreach ($eventCollection as $event) {
            $result[] = $this->getEventToClientModel($event);
        }
        return $result;
    }

    /**
     * @param EventInterface $event
     * @return array
     * @throws Exception
     */
    private function getEventToClientModel(EventInterface $event): array {
        return [
            self::ID => $event->getId(),
            self::NUM => $event->getNum(),
            self::NAME => $event->getName(),
            self::EVENT_TIME => $this->getEventTimeToClientModel($event->getEventTime()),
            self::EXTRA => $this->getEventExtraToClientModel($event->getEventExtra()),
            self::GROUP => $this->getGroupToClientModel($event->getGroup()),
            self::CLASSIFICATION => $this->getClassificationToClientModel($event->getClassification()),
            self::VENUE => $this->getVenueToClientModel($event->getVenue()),
            self::IMAGES => $this->getImageCollectionToClientModel($event->getImageCollection()),
            self::ORGANIZER => $this->getOrganizerToClientModel($event->getOrganizer()),
            self::PERFORMERS => $this->getPerformanceCollectionToClientModel($event->getPerformanceCollection()),
            self::PRICE => $this->getPriceToClientModel($event->getPrice())
        ];
    }

    /**
     * @param EventTimeInterface $eventTime
     * @return array
     */
    private function getEventTimeToClientModel(EventTimeInterface $eventTime): array {
        return [
            self::START_TIME => [
                self::LOCAL => $eventTime->getStartLocal()
            ],
            self::END_TIME => [
                self::LOCAL => $eventTime->getEndLocal()
            ]
        ];
    }

    /**
     * @param GroupInterface $group
     * @return array
     */
    private function getGroupToClientModel(GroupInterface $group): array {
        return [
            self::ID => $group->getId(),
            self::NAME => $group->getName()
        ];
    }

    /**
     * @param ClassificationInterface $classification
     * @return array
     */
    private function getClassificationToClientModel(ClassificationInterface $classification): array {
        return [
            self::ID => $classification->getId(),
            self::CODE => $classification->getCode(),
            self::NAME => $classification->getName(),
            self::SLUG => $classification->getSlug(),
            self::SEARCH_LINE => $classification->getSearchLine()
        ];
    }

    /**
     * @param VenueInterface $venue
     * @return array
     */
    private function getVenueToClientModel(VenueInterface $venue): array {
        return [
            self::ID => $venue->getId(),
            self::NAME => $venue->getName(),
            self::ADDRESS => $venue->getAddress(),
            self::LAT => $venue->getLat(),
            self::LONG => $venue->getLong(),
            self::COUNTRY => $this->getCountrToClientModel($venue->getCountry()),
            self::CITY => $this->getCityToClientModel($venue->getCity())
        ];
    }

    /**
     * @param CountryInterface $country
     * @return array
     */
    private function getCountrToClientModel(CountryInterface $country): array {
        return [
            self::CODE => $country->getCode(),
            self::NAME => $country->getName()
        ];
    }

    /**
     * @param CityInterface $city
     * @return array
     */
    private function getCityToClientModel(CityInterface $city): array {
        return [
            self::ID => $city->getId(),
            self::NAME => $city->getName()
        ];
    }

    /**
     * @param ImageCollectionInterface $imageCollection
     * @return array
     */
    private function getImageCollectionToClientModel(ImageCollectionInterface $imageCollection): array {
        $result = [];
        foreach ($imageCollection as $image) {
            $result[] = $this->getImageToClientModel($image);
        }
        return $result;
    }

    /**
     * @param ImageInterface $image
     * @return array
     */
    private function getImageToClientModel(ImageInterface $image): array {
        return [
            self::RATIO => $image->getRatio(),
            self::URL => $image->getUrl(),
            self::ORDER => $image->getOrder(),
            self::HEIGHT => $image->getHeight(),
            self::WIDTH => $image->getWidth(),
        ];
    }

    /**
     * @param OrganizerInterface $organizer
     * @return array
     */
    private function getOrganizerToClientModel(OrganizerInterface $organizer): array {
        return [
            self::NAME => $organizer->getName(),
            self::DESCRIPTION => $organizer->getDescription()
        ];
    }

    /**
     * @param EventExtraInterface $eventExtra
     * @return array
     */
    private function getEventExtraToClientModel(EventExtraInterface $eventExtra): array {
        return [
            self::DESCRIPTION => $eventExtra->getDescription(),
            self::INFORMRMATION => $eventExtra->getInformation(),
            self::ORGANIZER_NOTICE => $eventExtra->getOrganizerNotice(),
            self::BUTTON_BUY_TICKET => [
                self::COLOR => $eventExtra->getButtonBuyTicket()->getColor(),
                self::DESCRIPTION => $eventExtra->getButtonBuyTicket()->getDescription(),
                self::RECOMMENDATION => $eventExtra->getButtonBuyTicket()->getRecommendation(),
                self::CALL_TO_ACTION => $eventExtra->getButtonBuyTicket()->getCallToAction(),
                self::TICKET_URL => $eventExtra->getButtonBuyTicket()->getUrl()
            ]
        ];
    }

    /**
     * @param PerformanceCollectionInterface $performanceCollection
     * @return array
     * @throws Exception
     */
    private function getPerformanceCollectionToClientModel(PerformanceCollectionInterface $performanceCollection): array {
        $result = [];
        foreach ($performanceCollection as $performance) {
            $result[] = $this->getPerformanceToClientModel($performance);
        }
        return $result;
    }

    /**
     * @param PerformanceInterface $performance
     * @return array
     */
    private function getPerformanceToClientModel(PerformanceInterface $performance): array {
        return [
            self::NAME => $performance->getPerformer()->getName(),
            self::OFFICIAL_WEBSITE => $performance->getPerformer()->getOfficialWebsite(),
            self::PRIMARY => $performance->isPrimary()
        ];
    }

    /**
     * @param PriceInterface $price
     * @return array
     */
    private function getPriceToClientModel(PriceInterface $price): array {
        return [
            self::EUR => $price->getEur(),
            self::USD => $price->getUsd()
        ];
    }
}
