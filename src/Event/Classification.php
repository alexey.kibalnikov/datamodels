<?php
/**
 * Classification.php
 * @author Alexey Kibalnikov (alexey.kibalnikov@gmail.com)
 * @copyright (c) iWeekender 2019
 * @package DataModels\Event
 */

namespace iWeekender\DataModels\Event;

use iWeekender\DataModels\AbstractDataModelElement;
use iWeekender\Contract\DataModels\Event\ClassificationInterface;

/**
 * Class Classification
 */
final class Classification extends AbstractDataModelElement implements ClassificationInterface
{
    protected $propertyMapsToAssociativeArray = [
        'id',
        'parentId',
        'name',
        'code',
        'slug',
        'level',
        'searchLine'
    ];

    /**
     * @var int|null
     */
    private $id;

    /**
     * @var int|null
     */
    private $parentId;

    /**
     * @var string|null
     */
    private $name;

    /**
     * @var string|null
     */
    private $code;

    /**
     * @var string|null
     */
    private $slug;

    /**
     * @var int|null
     */
    private $level;

    /**
     * @var string|null
     */
    private $searchLine;

    /**
     * @inheritDoc
     */
    public function getId(): ?int {
        return $this->id;
    }

    /**
     * @inheritDoc
     */
    public function setId(?int $id): ClassificationInterface {
        $this->id = $id;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getParentId(): ?int {
        return $this->parentId;
    }

    /**
     * @inheritDoc
     */
    public function setParentId(?int $parentId): ClassificationInterface {
        $this->parentId = $parentId;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getName(): ?string {
        return $this->name;
    }

    /**
     * @inheritDoc
     */
    public function setName(?string $name): ClassificationInterface {
        $this->name = $name;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getCode(): ?string {
        return $this->code;
    }

    /**
     * @inheritDoc
     */
    public function setCode(?string $code): ClassificationInterface {
        $this->code = $code;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getSlug(): ?string {
        return $this->slug;
    }

    /**
     * @inheritDoc
     */
    public function setSlug(?string $slug): ClassificationInterface {
        $this->slug = $slug;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getLevel(): ?int {
        return $this->level;
    }

    /**
     * @inheritDoc
     */
    public function setLevel(?int $level): ClassificationInterface {
        $this->level = $level;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getSearchLine(): ?string {
        return $this->searchLine;
    }

    /**
     * @inheritDoc
     */
    public function setSearchLine(?string $searchLine): ClassificationInterface {
        $this->searchLine = $searchLine;
        return $this;
    }
}
