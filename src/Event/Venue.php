<?php
/**
 * Venue.php
 * @author Alexey Kibalnikov (alexey.kibalnikov@gmail.com)
 * @copyright (c) iWeekender 2019
 * @package DataModels\Event
 */

namespace iWeekender\DataModels\Event;

use iWeekender\Contract\DataModels\Common\CityInterface;
use iWeekender\Contract\DataModels\Common\CountryInterface;
use iWeekender\Contract\DataModels\Event\VenueInterface;
use iWeekender\DataModels\AbstractDataModelElement;
use iWeekender\DataModels\Common\City;
use iWeekender\DataModels\Common\Country;

/**
 * Class Venue
 */
final class Venue extends AbstractDataModelElement implements VenueInterface
{
    protected $propertyMapsToAssociativeArray = [
        'id',
        'name',
        'city',
        'country',
        'address',
        'lat',
        'long',
        'officialWebsite',
        'seatmapUrl',
        'source'
    ];

    protected $propertyMapsClass = [
        'city'    => City::class,
        'country' => Country::class
    ];

    /**
     * @var int|null
     */
    private $id;

    /**
     * @var string|null
     */
    private $name;

    /**
     * @var City
     */
    private $city;

    /**
     * @var Country
     */
    private $country;

    /**
     * @var string|null
     */
    private $address;

    /**
     * @var float
     */
    private $lat = self::DEFAULT_LAT_LONG;

    /**
     * @var float
     */
    private $long = self::DEFAULT_LAT_LONG;

    /**
     * @var string|null
     */
    private $officialWebsite;

    /**
     * @var string|null
     */
    private $seatmapUrl;

    /**
     * @var string|null
     */
    private $source;

    /**
     * @inheritDoc
     */
    public function getId(): ?int {
        return $this->id;
    }

    /**
     * @inheritDoc
     */
    public function setId(?int $id): VenueInterface {
        $this->id = $id;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getName(): ?string {
        return $this->name;
    }

    /**
     * @inheritDoc
     */
    public function setName(?string $name): VenueInterface {
        $this->name = $name;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getCity(): CityInterface {
        return $this->city;
    }

    /**
     * @inheritDoc
     */
    public function setCity(CityInterface $city): VenueInterface {
        $this->city = $city;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getCountry(): CountryInterface {
        return $this->country;
    }

    /**
     * @inheritDoc
     */
    public function setCountry(CountryInterface $country): VenueInterface {
        $this->country = $country;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getAddress(): ?string {
        return $this->address;
    }

    /**
     * @inheritDoc
     */
    public function setAddress(?string $address): VenueInterface {
        $this->address = $address;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getLat(): float {
        return $this->lat;
    }

    /**
     * @inheritDoc
     */
    public function setLat(float $lat): VenueInterface {
        $this->lat = $lat;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getLong(): float {
        return $this->long;
    }

    /**
     * @inheritDoc
     */
    public function setLong(float $long): VenueInterface {
        $this->long = $long;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getOfficialWebsite(): ?string {
        return $this->officialWebsite;
    }

    /**
     * @inheritDoc
     */
    public function setOfficialWebsite(?string $officialWebsite): VenueInterface {
        $this->officialWebsite = $officialWebsite;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getSeatmapUrl(): ?string {
        return $this->seatmapUrl;
    }

    /**
     * @inheritDoc
     */
    public function setSeatmapUrl(?string $seatmapUrl): VenueInterface {
        $this->seatmapUrl = $seatmapUrl;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getSource(): ?string {
        return $this->source;
    }

    /**
     * @inheritDoc
     */
    public function setSource(?string $source): VenueInterface {
        $this->source = $source;
        return $this;
    }
}
