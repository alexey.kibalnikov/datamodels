<?php
/**
 * Event.php
 * @author Alexey Kibalnikov (alexey.kibalnikov@gmail.com)
 * @copyright (c) iWeekender 2019
 * @package DataModels\Event
 */

namespace iWeekender\DataModels\Event;

use iWeekender\Contract\DataModels\Common\PriceInterface;
use iWeekender\Contract\DataModels\Event\ClassificationInterface;
use iWeekender\Contract\DataModels\Event\EventExtraInterface;
use iWeekender\Contract\DataModels\Event\EventInterface;
use iWeekender\Contract\DataModels\Event\EventTimeInterface;
use iWeekender\Contract\DataModels\Event\GroupInterface;
use iWeekender\Contract\DataModels\Event\ImageCollectionInterface;
use iWeekender\Contract\DataModels\Event\OrganizerInterface;
use iWeekender\Contract\DataModels\Event\PerformanceCollectionInterface;
use iWeekender\Contract\DataModels\Event\VenueInterface;
use iWeekender\DataModels\AbstractDataModelElement;
use iWeekender\DataModels\Common\Price;

/**
 * Class Event
 */
final class Event extends AbstractDataModelElement implements EventInterface
{
    protected $propertyMapsToAssociativeArray = [
        'id',
        'num',
        'supplierCode',
        'externalId',
        'name',
        'eventExtra',
        'eventTime',
        'venue',
        'classification',
        'group',
        'organizer',
        'performanceCollection',
        'imageCollection',
        'price'
    ];

    protected $propertyMapsClass = [
        'eventExtra'            => EventExtra::class,
        'eventTime'             => EventTime::class,
        'venue'                 => Venue::class,
        'classification'        => Classification::class,
        'group'                 => Group::class,
        'organizer'             => Organizer::class,
        'performanceCollection' => PerformanceCollection::class,
        'imageCollection'       => ImageCollection::class,
        'price'                 => Price::class
    ];

    /**
     * @var int|null
     */
    private $id;

    /**
     * @var int|null
     */
    private $num;

    /**
     * @var string|null
     */
    private $supplierCode;

    /**
     * @var string|null
     */
    private $externalId;

    /**
     * @var string|null
     */
    private $name;

    /**
     * @var EventExtra
     */
    private $eventExtra;

    /**
     * @var EventTime
     */
    private $eventTime;

    /**
     * @var Venue
     */
    private $venue;

    /**
     * @var Classification|null
     */
    private $classification;

    /**
     * @var Group
     */
    private $group;

    /**
     * @var Organizer|null
     */
    private $organizer;

    /**
     * @var PerformanceCollection|null
     */
    private $performanceCollection;

    /**
     * @var ImageCollection|null
     */
    private $imageCollection;

    /**
     * @var Price
     */
    private $price;

    /**
     * @inheritDoc
     */
    public function getId(): ?int {
        return $this->id;
    }

    /**
     * @inheritDoc
     */
    public function setId(?int $id): EventInterface {
        $this->id = $id;

        if (isset($id)) {
            if (!is_null($this->getImageCollection())) {
                $this->getImageCollection()->setEventId($id);
            }

            if (!is_null($this->getPerformanceCollection())) {
                $this->getImageCollection()->setEventId($id);
            }

            if (!is_null($this->getEventExtra())) {
                $this->getEventExtra()->setEventId($id);
            }

            if (!is_null($this->getEventTime())) {
                $this->getEventTime()->setEventId($id);
            }

            if (!is_null($this->getPrice())) {
                $this->getPrice()->setId($id);
            }
        }

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getNum(): ?int {
        return $this->num;
    }

    /**
     * @inheritDoc
     */
    public function setNum(?int $num): EventInterface {
        $this->num = $num;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getSupplierCode(): ?string {
        return $this->supplierCode;
    }

    /**
     * @inheritDoc
     */
    public function setSupplierCode(?string $supplierCode): EventInterface {
        $this->supplierCode = $supplierCode;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getExternalId(): ?string {
        return $this->externalId;
    }

    /**
     * @inheritDoc
     */
    public function setExternalId(?string $externalId): EventInterface {
        $this->externalId = $externalId;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getName(): ?string {
        return $this->name;
    }

    /**
     * @inheritDoc
     */
    public function setName(?string $name): EventInterface {
        $this->name = $name;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getEventExtra(): EventExtraInterface {
        return $this->eventExtra;
    }

    /**
     * @inheritDoc
     */
    public function setEventExtra(EventExtraInterface $eventExtra): EventInterface {
        $this->eventExtra = $eventExtra;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getEventTime(): EventTimeInterface {
        return $this->eventTime;
    }

    /**
     * @inheritDoc
     */
    public function setEventTime(EventTimeInterface $eventTime): EventInterface {
        $this->eventTime = $eventTime;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getVenue(): VenueInterface {
        return $this->venue;
    }

    /**
     * @inheritDoc
     */
    public function setVenue(VenueInterface $venue): EventInterface {
        $this->venue = $venue;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getClassification(): ?ClassificationInterface {
        return $this->classification;
    }

    /**
     * @inheritDoc
     */
    public function setClassification(ClassificationInterface $classification): EventInterface {
        $this->classification = $classification;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getGroup(): GroupInterface {
        return $this->group;
    }

    /**
     * @inheritDoc
     */
    public function setGroup(GroupInterface $group): EventInterface {
        $this->group = $group;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getOrganizer(): ?OrganizerInterface {
        return $this->organizer;
    }

    /**
     * @inheritDoc
     */
    public function setOrganizer(OrganizerInterface $organizer): EventInterface {
        $this->organizer = $organizer;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getPerformanceCollection(): ?PerformanceCollectionInterface {
        return $this->performanceCollection;
    }

    /**
     * @inheritDoc
     */
    public function setPerformanceCollection(PerformanceCollectionInterface $performanceCollection): EventInterface {
        $this->performanceCollection = $performanceCollection;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getImageCollection(): ?ImageCollectionInterface {
        return $this->imageCollection;
    }

    /**
     * @inheritDoc
     */
    public function setImageCollection(ImageCollectionInterface $imageCollection): EventInterface {
        $this->imageCollection = $imageCollection;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getPrice(): PriceInterface {
        return $this->price;
    }

    /**
     * @inheritDoc
     */
    public function setPrice(PriceInterface $price): EventInterface {
        $this->price = $price;
        return $this;
    }
}
